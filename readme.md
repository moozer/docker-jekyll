docker-jekyll
=============

Basic docker container for using jekyll in gitlab ci.

It is intended to make creation of jekyll static sitesfaster, since mostof the time spend i to install the stuff needed.


Adding docker images to gitlab.com
----------------

```
# don't show your token to anyone
export CI_TOKEN=asdfghjkl1234

# login to gitlabs docker hub
docker login -u gitlab-ci-token -p $CI_TOKEN registry.gitlab.com

# build and puch it
docker build -t registry.gitlab.com/moozer/docker-jekyl .
docker push registry.gitlab.com/moozer/docker-jekyll
```

The ci in this repo does it automatically.

gitlab.com has some very good reference material for docker+CI, e.g. [here](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#container-registry-examples)

To use it
----------------

add a line like this in `.gitlab-ci.yml`:

```
...
image: registry.gitlab.com/moozer/docker-jekyll:latest
...
```

It is automatically rebuild every month - there are some tests, but still stuff might break for you. Please create an issue is that is the case.
